﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="11008008">
	<Property Name="NI.Lib.Icon" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;7R=2MR%!81N=?"5Q&lt;/07RB7W!,&lt;'&amp;&lt;9+K1,7Q,&lt;)%N&lt;!NMA3X)DW?-RJ(JQ"I\%%Z,(@`BA#==ZB3RN;]28_,V7@PWW`:R`&gt;HV*SU_WE@\N_XF[3:^^TX\+2YP)D7K6;G-RV3P)R`ZS%=_]J'XP/5N&lt;XH,7V\SEJ?]Z#5P?=J4HP+5JTTFWS%0?=B$DD1G(R/.1==!IT.+D)`B':\B'2Z@9XC':XC':XBUC?%:HO%:HO&amp;R7QT0]!T0]!S0I4&lt;*&lt;)?=:XA-(]X40-X40-VDSGC?"GC4N9(&lt;)"D2,L;4ZGG?ZH%;T&gt;-]T&gt;-]T?.S.%`T.%`T.)^&lt;NF8J4@-YZ$S'C?)JHO)JHO)R&gt;"20]220]230[;*YCK=ASI2F=)1I.Z5/Z5PR&amp;)^@54T&amp;5TT&amp;5TQO&lt;5_INJ6Z;"[(H#&gt;ZEC&gt;ZEC&gt;Z$"(*ETT*ETT*9^B)HO2*HO2*(F.&amp;]C20]C2)GN4UE1:,.[:/+5A?0^NOS?UJ^3&lt;*\9B9GT@7JISVW7*NIFC&lt;)^:$D`5Q9TWE7)M@;V&amp;D,6;M29DVR]6#R],%GC47T9_/=@&gt;Z5V&gt;V57&gt;V5E&gt;V5(OV?^T[FTP?\`?YX7ZRP6\D=LH%_8S/U_E5R_-R$I&gt;$\0@\W/VW&lt;[_"\Y[X&amp;],0^^+,]T_J&gt;`J@_B_]'_.T`$KO.@I"XC-_N!!!!!!</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Item Name="Private" Type="Folder">
		<Item Name="Default Instrument Setup.vi" Type="VI" URL="../Private/Default Instrument Setup.vi"/>
		<Item Name="Read.vi" Type="VI" URL="../Private/Read.vi"/>
		<Item Name="Write.vi" Type="VI" URL="../Private/Write.vi"/>
	</Item>
	<Item Name="Public" Type="Folder">
		<Item Name="Action-Status" Type="Folder">
			<Item Name="File Handling" Type="Folder">
				<Item Name="Clear Channel State.vi" Type="VI" URL="../Public/Action-Status/File Handling/Clear Channel State.vi"/>
				<Item Name="Directory Content.vi" Type="VI" URL="../Public/Action-Status/File Handling/Directory Content.vi"/>
				<Item Name="File Manager.vi" Type="VI" URL="../Public/Action-Status/File Handling/File Manager.vi"/>
				<Item Name="File Transfer From a Storage Device.vi" Type="VI" URL="../Public/Action-Status/File Handling/File Transfer From a Storage Device.vi"/>
				<Item Name="File Transfer To a Storage Device.vi" Type="VI" URL="../Public/Action-Status/File Handling/File Transfer To a Storage Device.vi"/>
				<Item Name="Recall Channel Coefficient.vi" Type="VI" URL="../Public/Action-Status/File Handling/Recall Channel Coefficient.vi"/>
				<Item Name="Recall Channel State.vi" Type="VI" URL="../Public/Action-Status/File Handling/Recall Channel State.vi"/>
				<Item Name="Recall.vi" Type="VI" URL="../Public/Action-Status/File Handling/Recall.vi"/>
				<Item Name="Store Channel Coefficient.vi" Type="VI" URL="../Public/Action-Status/File Handling/Store Channel Coefficient.vi"/>
				<Item Name="Store Channel State.vi" Type="VI" URL="../Public/Action-Status/File Handling/Store Channel State.vi"/>
				<Item Name="Store Instrument State.vi" Type="VI" URL="../Public/Action-Status/File Handling/Store Instrument State.vi"/>
				<Item Name="Store.vi" Type="VI" URL="../Public/Action-Status/File Handling/Store.vi"/>
			</Item>
			<Item Name="System" Type="Folder">
				<Item Name="Hardcopy.vi" Type="VI" URL="../Public/Action-Status/System/Hardcopy.vi"/>
				<Item Name="Read System Information.vi" Type="VI" URL="../Public/Action-Status/System/Read System Information.vi"/>
				<Item Name="Read Warmed-Up State.vi" Type="VI" URL="../Public/Action-Status/System/Read Warmed-Up State.vi"/>
				<Item Name="Security Level.vi" Type="VI" URL="../Public/Action-Status/System/Security Level.vi"/>
				<Item Name="Shutdown.vi" Type="VI" URL="../Public/Action-Status/System/Shutdown.vi"/>
			</Item>
			<Item Name="Abort Sweep.vi" Type="VI" URL="../Public/Action-Status/Abort Sweep.vi"/>
			<Item Name="Autoscale Y Axis.vi" Type="VI" URL="../Public/Action-Status/Autoscale Y Axis.vi"/>
			<Item Name="Calculate Calibration Coefficients.vi" Type="VI" URL="../Public/Action-Status/Calculate Calibration Coefficients.vi"/>
			<Item Name="Calculate Receiver Calibration Coefficients.vi" Type="VI" URL="../Public/Action-Status/Calculate Receiver Calibration Coefficients.vi"/>
			<Item Name="Calculate Scalar-Mixer Calibration Coefficients.vi" Type="VI" URL="../Public/Action-Status/Calculate Scalar-Mixer Calibration Coefficients.vi"/>
			<Item Name="Calibration Save.vi" Type="VI" URL="../Public/Action-Status/Calibration Save.vi"/>
			<Item Name="Clear Averaging Data.vi" Type="VI" URL="../Public/Action-Status/Clear Averaging Data.vi"/>
			<Item Name="Clear Calibration Data.vi" Type="VI" URL="../Public/Action-Status/Clear Calibration Data.vi"/>
			<Item Name="Clear Scalar-Mixer Calibration Data.vi" Type="VI" URL="../Public/Action-Status/Clear Scalar-Mixer Calibration Data.vi"/>
			<Item Name="Copy Measurement Data to MEM.vi" Type="VI" URL="../Public/Action-Status/Copy Measurement Data to MEM.vi"/>
			<Item Name="Execute Analysis.vi" Type="VI" URL="../Public/Action-Status/Execute Analysis.vi"/>
			<Item Name="Execute ECal.vi" Type="VI" URL="../Public/Action-Status/Execute ECal.vi"/>
			<Item Name="Execute Scalar-Mixer ECal.vi" Type="VI" URL="../Public/Action-Status/Execute Scalar-Mixer ECal.vi"/>
			<Item Name="Marker Search.vi" Type="VI" URL="../Public/Action-Status/Marker Search.vi"/>
			<Item Name="Match Frequency Range.vi" Type="VI" URL="../Public/Action-Status/Match Frequency Range.vi"/>
			<Item Name="Measure Calibration Data.vi" Type="VI" URL="../Public/Action-Status/Measure Calibration Data.vi"/>
			<Item Name="Measure Power Calibration Data.vi" Type="VI" URL="../Public/Action-Status/Measure Power Calibration Data.vi"/>
			<Item Name="Measure Scalar-Mixer Calibration Data.vi" Type="VI" URL="../Public/Action-Status/Measure Scalar-Mixer Calibration Data.vi"/>
			<Item Name="Reset Calibration Kit.vi" Type="VI" URL="../Public/Action-Status/Reset Calibration Kit.vi"/>
			<Item Name="Save Cal Done.vi" Type="VI" URL="../Public/Action-Status/Save Cal Done.vi"/>
			<Item Name="Send Trigger.vi" Type="VI" URL="../Public/Action-Status/Send Trigger.vi"/>
			<Item Name="Set Marker Position.vi" Type="VI" URL="../Public/Action-Status/Set Marker Position.vi"/>
			<Item Name="Set Marker Values.vi" Type="VI" URL="../Public/Action-Status/Set Marker Values.vi"/>
			<Item Name="SingleSweepOPC.vi" Type="VI" URL="../Public/Action-Status/SingleSweepOPC.vi"/>
			<Item Name="Start Sweep.vi" Type="VI" URL="../Public/Action-Status/Start Sweep.vi"/>
			<Item Name="Wait For Operation Complete.vi" Type="VI" URL="../Public/Action-Status/Wait For Operation Complete.vi"/>
		</Item>
		<Item Name="Configure" Type="Folder">
			<Item Name="Analysis Configuration" Type="Folder">
				<Item Name="Configure Analysis.vi" Type="VI" URL="../Public/Configure/Analysis Configuration/Configure Analysis.vi"/>
			</Item>
			<Item Name="Calibration Configuration" Type="Folder">
				<Item Name="Configure Adapter Removal.vi" Type="VI" URL="../Public/Configure/Calibration Configuration/Configure Adapter Removal.vi"/>
				<Item Name="Configure Calibration Kit.vi" Type="VI" URL="../Public/Configure/Calibration Configuration/Configure Calibration Kit.vi"/>
				<Item Name="Configure Calibration.vi" Type="VI" URL="../Public/Configure/Calibration Configuration/Configure Calibration.vi"/>
				<Item Name="Configure Class.vi" Type="VI" URL="../Public/Configure/Calibration Configuration/Configure Class.vi"/>
				<Item Name="Configure Electrical Calibration.vi" Type="VI" URL="../Public/Configure/Calibration Configuration/Configure Electrical Calibration.vi"/>
				<Item Name="Configure Port Extension.vi" Type="VI" URL="../Public/Configure/Calibration Configuration/Configure Port Extension.vi"/>
				<Item Name="Configure Receiver Calibration.vi" Type="VI" URL="../Public/Configure/Calibration Configuration/Configure Receiver Calibration.vi"/>
				<Item Name="Configure Scalar Mixer Calibration.vi" Type="VI" URL="../Public/Configure/Calibration Configuration/Configure Scalar Mixer Calibration.vi"/>
				<Item Name="Configure Standard Offsets.vi" Type="VI" URL="../Public/Configure/Calibration Configuration/Configure Standard Offsets.vi"/>
			</Item>
			<Item Name="Fixture Simulator Configuration" Type="Folder">
				<Item Name="Configure 4-port Network (De-)Embedding.vi" Type="VI" URL="../Public/Configure/Fixture Simulator Configuration/Configure 4-port Network (De-)Embedding.vi"/>
				<Item Name="Configure Balance Unbalance Conversion.vi" Type="VI" URL="../Public/Configure/Fixture Simulator Configuration/Configure Balance Unbalance Conversion.vi"/>
				<Item Name="Configure Fixture Simulator.vi" Type="VI" URL="../Public/Configure/Fixture Simulator Configuration/Configure Fixture Simulator.vi"/>
				<Item Name="Configure Matching Circuit Embedding.vi" Type="VI" URL="../Public/Configure/Fixture Simulator Configuration/Configure Matching Circuit Embedding.vi"/>
				<Item Name="Configure Network De-embedding.vi" Type="VI" URL="../Public/Configure/Fixture Simulator Configuration/Configure Network De-embedding.vi"/>
				<Item Name="Configure Port Impedance Conversion.vi" Type="VI" URL="../Public/Configure/Fixture Simulator Configuration/Configure Port Impedance Conversion.vi"/>
			</Item>
			<Item Name="Limit Test" Type="Folder">
				<Item Name="Configure Limit Test Table.vi" Type="VI" URL="../Public/Configure/Limit Test/Configure Limit Test Table.vi"/>
				<Item Name="Configure Limit Test.vi" Type="VI" URL="../Public/Configure/Limit Test/Configure Limit Test.vi"/>
			</Item>
			<Item Name="Marker Configuration" Type="Folder">
				<Item Name="Configure Active Marker.vi" Type="VI" URL="../Public/Configure/Marker Configuration/Configure Active Marker.vi"/>
				<Item Name="Configure Bandwidth Search.vi" Type="VI" URL="../Public/Configure/Marker Configuration/Configure Bandwidth Search.vi"/>
				<Item Name="Configure Marker Function.vi" Type="VI" URL="../Public/Configure/Marker Configuration/Configure Marker Function.vi"/>
				<Item Name="Configure Marker Search.vi" Type="VI" URL="../Public/Configure/Marker Configuration/Configure Marker Search.vi"/>
				<Item Name="Configure Marker.vi" Type="VI" URL="../Public/Configure/Marker Configuration/Configure Marker.vi"/>
				<Item Name="Configure Notch Search.vi" Type="VI" URL="../Public/Configure/Marker Configuration/Configure Notch Search.vi"/>
				<Item Name="Marker Mode.vi" Type="VI" URL="../Public/Configure/Marker Configuration/Marker Mode.vi"/>
			</Item>
			<Item Name="Measurement Conditions" Type="Folder">
				<Item Name="Configure Absolute Measurement.vi" Type="VI" URL="../Public/Configure/Measurement Conditions/Configure Absolute Measurement.vi"/>
				<Item Name="Configure AUX Measurement.vi" Type="VI" URL="../Public/Configure/Measurement Conditions/Configure AUX Measurement.vi"/>
				<Item Name="Configure Averaging.vi" Type="VI" URL="../Public/Configure/Measurement Conditions/Configure Averaging.vi"/>
				<Item Name="Configure Channel And Trace.vi" Type="VI" URL="../Public/Configure/Measurement Conditions/Configure Channel And Trace.vi"/>
				<Item Name="Configure External Signal Source.vi" Type="VI" URL="../Public/Configure/Measurement Conditions/Configure External Signal Source.vi"/>
				<Item Name="Configure Fixed Frequency.vi" Type="VI" URL="../Public/Configure/Measurement Conditions/Configure Fixed Frequency.vi"/>
				<Item Name="Configure Frequency Offset.vi" Type="VI" URL="../Public/Configure/Measurement Conditions/Configure Frequency Offset.vi"/>
				<Item Name="Configure IF Bandwidth.vi" Type="VI" URL="../Public/Configure/Measurement Conditions/Configure IF Bandwidth.vi"/>
				<Item Name="Configure Power.vi" Type="VI" URL="../Public/Configure/Measurement Conditions/Configure Power.vi"/>
				<Item Name="Configure RF OnOff.vi" Type="VI" URL="../Public/Configure/Measurement Conditions/Configure RF OnOff.vi"/>
				<Item Name="Configure Smoothing.vi" Type="VI" URL="../Public/Configure/Measurement Conditions/Configure Smoothing.vi"/>
				<Item Name="Configure Sweep Stimulus.vi" Type="VI" URL="../Public/Configure/Measurement Conditions/Configure Sweep Stimulus.vi"/>
				<Item Name="Configure Sweep.vi" Type="VI" URL="../Public/Configure/Measurement Conditions/Configure Sweep.vi"/>
				<Item Name="Current Consumption.seq" Type="Document" URL="../Public/Configure/Measurement Conditions/Current Consumption.seq"/>
				<Item Name="Define Sweep Segments Table.vi" Type="VI" URL="../Public/Configure/Measurement Conditions/Define Sweep Segments Table.vi"/>
			</Item>
			<Item Name="Power Calibration" Type="Folder">
				<Item Name="Configure Calibration Factor Table.vi" Type="VI" URL="../Public/Configure/Power Calibration/Configure Calibration Factor Table.vi"/>
				<Item Name="Configure Loss Compensation Table.vi" Type="VI" URL="../Public/Configure/Power Calibration/Configure Loss Compensation Table.vi"/>
				<Item Name="Configure Power Calibration.vi" Type="VI" URL="../Public/Configure/Power Calibration/Configure Power Calibration.vi"/>
				<Item Name="Configure Reference Calibration Factor.vi" Type="VI" URL="../Public/Configure/Power Calibration/Configure Reference Calibration Factor.vi"/>
			</Item>
			<Item Name="Screen Display" Type="Folder">
				<Item Name="Configure Corrections.vi" Type="VI" URL="../Public/Configure/Screen Display/Configure Corrections.vi"/>
				<Item Name="Configure Display Color.vi" Type="VI" URL="../Public/Configure/Screen Display/Configure Display Color.vi"/>
				<Item Name="Configure Display Layout.vi" Type="VI" URL="../Public/Configure/Screen Display/Configure Display Layout.vi"/>
				<Item Name="Configure Display.vi" Type="VI" URL="../Public/Configure/Screen Display/Configure Display.vi"/>
				<Item Name="Configure Scale.vi" Type="VI" URL="../Public/Configure/Screen Display/Configure Scale.vi"/>
				<Item Name="Configure Trace Display.vi" Type="VI" URL="../Public/Configure/Screen Display/Configure Trace Display.vi"/>
				<Item Name="Configure X Axis For Segment Sweep.vi" Type="VI" URL="../Public/Configure/Screen Display/Configure X Axis For Segment Sweep.vi"/>
			</Item>
			<Item Name="System Configuration" Type="Folder">
				<Item Name="Configure Controlling E5091A.vi" Type="VI" URL="../Public/Configure/System Configuration/Configure Controlling E5091A.vi"/>
				<Item Name="Configure Correction and Temperature.vi" Type="VI" URL="../Public/Configure/System Configuration/Configure Correction and Temperature.vi"/>
				<Item Name="Configure Date And Time.vi" Type="VI" URL="../Public/Configure/System Configuration/Configure Date And Time.vi"/>
				<Item Name="Configure External Signal Generator.vi" Type="VI" URL="../Public/Configure/System Configuration/Configure External Signal Generator.vi"/>
				<Item Name="Configure Handler IO Control.vi" Type="VI" URL="../Public/Configure/System Configuration/Configure Handler IO Control.vi"/>
				<Item Name="Configure Initial Source Port.vi" Type="VI" URL="../Public/Configure/System Configuration/Configure Initial Source Port.vi"/>
				<Item Name="Configure Parameter Conversion.vi" Type="VI" URL="../Public/Configure/System Configuration/Configure Parameter Conversion.vi"/>
			</Item>
			<Item Name="Time Domain Configuration" Type="Folder">
				<Item Name="Configure Time Domain Display Range.vi" Type="VI" URL="../Public/Configure/Time Domain Configuration/Configure Time Domain Display Range.vi"/>
				<Item Name="Configure Time Domain Gating.vi" Type="VI" URL="../Public/Configure/Time Domain Configuration/Configure Time Domain Gating.vi"/>
				<Item Name="Configure Time Domain Transform.vi" Type="VI" URL="../Public/Configure/Time Domain Configuration/Configure Time Domain Transform.vi"/>
			</Item>
			<Item Name="Trigger" Type="Folder">
				<Item Name="Configure External Trigger.vi" Type="VI" URL="../Public/Configure/Trigger/Configure External Trigger.vi"/>
				<Item Name="Configure Trigger.vi" Type="VI" URL="../Public/Configure/Trigger/Configure Trigger.vi"/>
			</Item>
		</Item>
		<Item Name="Data" Type="Folder">
			<Item Name="Limit Fail.vi" Type="VI" URL="../Public/Data/Limit Fail.vi"/>
			<Item Name="Marker Bandwidth Search Result.vi" Type="VI" URL="../Public/Data/Marker Bandwidth Search Result.vi"/>
			<Item Name="Marker Data.vi" Type="VI" URL="../Public/Data/Marker Data.vi"/>
			<Item Name="Marker Notch Search Result.vi" Type="VI" URL="../Public/Data/Marker Notch Search Result.vi"/>
			<Item Name="Math Marker Data.vi" Type="VI" URL="../Public/Data/Math Marker Data.vi"/>
			<Item Name="Read Analysis Results.vi" Type="VI" URL="../Public/Data/Read Analysis Results.vi"/>
			<Item Name="Read Calibration Data Array.vi" Type="VI" URL="../Public/Data/Read Calibration Data Array.vi"/>
			<Item Name="Read Data and Memory Array.vi" Type="VI" URL="../Public/Data/Read Data and Memory Array.vi"/>
			<Item Name="Read Fail Meas Points.vi" Type="VI" URL="../Public/Data/Read Fail Meas Points.vi"/>
			<Item Name="Read Handler Input Data.vi" Type="VI" URL="../Public/Data/Read Handler Input Data.vi"/>
			<Item Name="Read Stimulus Array.vi" Type="VI" URL="../Public/Data/Read Stimulus Array.vi"/>
			<Item Name="Trace Statistic Analysis Result.vi" Type="VI" URL="../Public/Data/Trace Statistic Analysis Result.vi"/>
			<Item Name="Write Calibration Data Array.vi" Type="VI" URL="../Public/Data/Write Calibration Data Array.vi"/>
			<Item Name="Write Data and Memory Array.vi" Type="VI" URL="../Public/Data/Write Data and Memory Array.vi"/>
			<Item Name="Write Handler Output Data.vi" Type="VI" URL="../Public/Data/Write Handler Output Data.vi"/>
		</Item>
		<Item Name="Utility" Type="Folder">
			<Item Name="Error Query.vi" Type="VI" URL="../Public/Utility/Error Query.vi"/>
			<Item Name="Lock Instrument.vi" Type="VI" URL="../Public/Utility/Lock Instrument.vi"/>
			<Item Name="Reset.vi" Type="VI" URL="../Public/Utility/Reset.vi"/>
			<Item Name="Revision Query.vi" Type="VI" URL="../Public/Utility/Revision Query.vi"/>
		</Item>
		<Item Name="Close.vi" Type="VI" URL="../Public/Close.vi"/>
		<Item Name="Initialize.vi" Type="VI" URL="../Public/Initialize.vi"/>
		<Item Name="VI Tree.vi" Type="VI" URL="../Public/VI Tree.vi"/>
	</Item>
	<Item Name="Agilent ENA Series Readme.html" Type="Document" URL="../Agilent ENA Series Readme.html"/>
</Library>
